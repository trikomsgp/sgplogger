var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'SGP Logger Node',
  script: 'C:\\Users\\Trikom\\Desktop\\Proyectos\\serverNode\\OPCClient\\index.js',
});

// Listen for the "uninstall" event so we know when it's done.
svc.on('uninstall',function(){
  //('Uninstall complete.');
  //('The service exists: ',svc.exists);
});

// Uninstall the service.
svc.uninstall();