let clients = [];

function updateClients(data){
    let index = clients.findIndex(c => c.id == data.id)
    if(index != -1){
        clients[index].state = data.state
    }
}

function saveClients(data = []) {
    clients = data
}

function getClients(){
    return clients
}

module.exports = {
    getClients,
    updateClients,
    saveClients
}