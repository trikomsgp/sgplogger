const { OPCUAClient, AttributeIds, resolveNodeId, TimestampsToReturn, MessageSecurityMode, SecurityPolicy, UserTokenType, UserIdentityToken} = require("node-opcua");
const async = require("async");

// Variables del Log4JS
const log4JS = require('./log4JS');
const logger = log4JS.getLogger();
const path = require('path');
const fileName = path.basename(__filename);

let funciones = require('./funciones');
let cacheClients = require('./cache/cache');

let clients = [];
let sessions = [];
let subscriptions = [];
let contador = 0;

let monitorTriggerTags = [];

async function connect(datosConexion) {
    let opcMiddleware = require("./controllers/opcMiddleware");
    logger.debug(fileName + "- Connect - " + " Inicio", datosConexion);

    let clientExist = clients.find(data => data.id == datosConexion.id);
    return new Promise(async (resolve, reject) => {
        if (!clientExist) {
            let token = {
                // UserIdentityInfoUserName: {
                    type: UserTokenType.UserName,
                    userName: 'admin',
                    password: 'ed7590d4'
                // }
            }

            let client = OPCUAClient.create({
                endpointMustExist: false,
                // keepSessionAlive: true,
                // securityMode: MessageSecurityMode.Sign,
                // securityPolicy: SecurityPolicy.Basic256Sha256,
                // serverCertificate: testClient._serverEndpoints[1].serverCertificate
            });

            // await client.connect(datosConexion.endpointUrl)
            // try {
            //     client.createSession(token, function(err, session) {
            //         if (err) {
            //             console.log('createSession', err)
            //         }
            //         else {
            //             // the_session = session;
            //             // sessions.push({session: session, idClient: client.id})
            //             console.log('session agregada', session)
            //         }
            //     });
                
            // } catch (error) {
            //     console.log('createSession', error)                
            // }

            client.on("backoff", (retry, delay) => {
                logger.debug(fileName + 
                    "- Connect - " + 
                    "still trying to connect to " + 
                    datosConexion.endpointUrl + 
                    ": retry =" + retry + "next attempt in " + 
                    delay / 1000 + 
                    "seconds"
                ); 
                resolve("Conexión iniciada.");
            });
        
            client.on("connection_lost", function (err) {
                console.log(fileName + "- Connect - connection lost", err);
                logger.debug(fileName + "- Connect - connection lost", err);
                cacheClients.updateClients({id: datosConexion.id, state: false});
                opcMiddleware.updateServerStatus(cacheClients.getClients());
            });
        
            client.on("connection_reestablished", function (err) {
                console.log(fileName + "- Connect - connection re-established", err);
                logger.debug(fileName + "- Connect - connection re-established", err);
                cacheClients.updateClients({id: datosConexion.id, state: true});
                opcMiddleware.updateServerStatus(cacheClients.getClients());
            });
        
            client.on("connection_failed", function (err) {
                if(err) {
                    logger.debug(fileName + "- Connect - " + "Connection failed");
                }
            });

            client.on("start_reconnection", function (err) {
                if(err) {
                    logger.debug(fileName + "- Connect - " + "Starting reconnection Failed");
                }
                else {
                    // logger.debug(fileName + "- Connect - "+"Starting reconnection", datosConexion);
                }
            });
        
            client.on("after_reconnection", (err) => {
                if (err) {
                    logger.debug(fileName + "- Connect - " + "Failed After Reconnection event =>", err);
                }
                else {
                    // logger.debug(fileName + "- Connect - "+"After Reconnection event ", datosConexion);
                }
            });

            client.on("reconnection_attempt_has_failed", function (err) {
                if(err) {
                    logger.debug(fileName + "- Connect - " + "Reconnection Attempt failed. Server shutdown.")
                }
            });
    
            client.on("abort", function (err) {
                logger.debug(fileName + "- Connect - " + "abort", err);
            })

            client.on("close", function (err) {
                logger.debug(fileName + "- Connect - " + "close", err)
                try {
                    client.disconnect();
                    console.log("close");
                }
                catch (err) {
                    logger.debug(fileName + "- Connect - " + "disconnect", err);
                }
            })

            client.on("connected", function (err) {
                logger.debug(fileName + "- Connect - " + "connected", err);
            })

            // client.on("send_chunk", function (err) {
            //     console.log("send_chunk", err)
            // })

            // client.on("receive_chunk", function (err) {
            //     console.log("receive_chunk", err)
            //     // clients.push({client: client, id: datosConexion.id})
            //     // resolve("connected")
            // })

            // client.on("send_request", function (err) {
            //     console.log("send_request", err)
            // })

            // client.on("receive_response", function (err) {
            //     console.log("receive_response", err)
            // })

            // client.on("lifetime_75", function (err) {
            //     console.log("lifetime_75", err)
            // })

            // client.on("security_token_renewed", function (err) {
            //     console.log("security_token_renewed", err)
            // })

            // client.on("timed_out_request", function (err) {
            //     console.log("timed_out_request", err)
            // })

            try {
                client.connect(datosConexion.endpointUrl, function(err) {
                    if (err) {
                        logger.debug(fileName + "- Connect - " + " cannot connect to endpoint :", datosConexion.endpointUrl);
                        resolve("connection failed");
                    }
                    else {
                        logger.debug(fileName + '- Connect - connect succesful', datosConexion);
                        clients.push({client: client, id: datosConexion.id});
                        cacheClients.updateClients({id: datosConexion.id, state: true});
                        opcMiddleware.updateServerStatus(cacheClients.getClients());
                        resolve("connected");
                    }
                });
            }
            catch (error) {
                logger.error(fileName + "- Connect - ", error);
                reject(error);
            }
            finally {
            }
        }
        else {
            resolve("Server already connected");
        }
    })
}

function createSession(datosConexion) {
    logger.debug(fileName + '- Connect - createSession', datosConexion);
    let client = clients.find(data => data.id == datosConexion.id);
    let session = sessions.find(data => data.idClient == datosConexion.id);

    return new Promise(async (resolve, reject) => {
        if(client && !session) {
            try {
                let token = {
                    type: UserTokenType.UserName,
                    userName: 'admin',
                    password: 'ed7590d4'
                }
                client.client.createSession( function(err, session) {
                    if (err) {
                        reject(err);
                    }
                    else {
                        // the_session = session;
                        sessions.push({session: session, idClient: client.id});
                        logger.debug('session agregada');
                        resolve(true);
                    }
                });
            }
            catch (error) {
                logger.error(fileName + '- createSession - ', error);
                reject(error);
            }
            finally {
            }
        }
        else {
            if(!client) {
                resolve("createSession - Cliente no encontrado.");
            }
            else if (client && session) {
                resolve("createSession - Cliente y Session ya creado.");
            }
        }
    })
}

function getData(datosConexion) {
    let dato = sessions.find(data => data.idClient == datosConexion.idClient);

    return new Promise(async (resolve, reject) => {
        if(dato) {
            try {
                dato.session.read({nodeId: datosConexion.nodeId, attributeId: AttributeIds.Value}, (err, dataValue) => {
                    if (!err) {
                        logger.debug(fileName + ' - getData - ', dataValue.toString());
                        let data = {
                            Valor: dataValue.value.value,
                            TipoDato: dataValue.value.dataType,
                            Calidad: dataValue.statusCode.value == 0 ? 192 : 0,
                            Fecha: funciones.getDateTime(JSON.parse(JSON.stringify(dataValue.sourceTimestamp)))
                        }
                        resolve(data)
                    }
                    else {
                        logger.error(fileName + '- getData - ', err);
                        reject(err);
                    }
                });
            }
            catch (error) {
                logger.error(fileName + ' - getData - ', error);
                reject(error);
            }
            finally {
            }
        }
        else {
            resolve("getData - Cliente no encontrado.");
        }
    })
}

function writeSingleNode(datosConexion) {
    let dato = sessions.find(data => data.idClient == datosConexion.id)
    let datatypes = {
        17: "Int32",
        18: "Double",
        19: "Boolean",
        20: "String"
    }
    return new Promise(async (resolve, reject) => {
        if(dato && datatypes[datosConexion.dataType]) {
            try {
                dato.session.writeSingleNode (datosConexion.nodeId, {dataType: datatypes[datosConexion.dataType], value: datosConexion.value},
                    function(err, statusCode, diagnosticInfo) {
                        if (!err) {
                            logger.debug(fileName + '- writeSingleNode - Escritura realizada con éxito.', datosConexion);
                            resolve(true);
                        } else {
                            logger.debug(fileName + '- writeSingleNode - Error en la escritura.', err);
                            resolve(false);
                        }
                    }
                );
            }
            catch (error) {
                logger.error(fileName + '- writeSingleNode - ', error);
                reject(error);
            }
        }
        else {
            if(!dato) {
                logger.debug(fileName + '- writeSingleNode - Cliente no encontrado.');
                resolve("writeSingleNode - Cliente no encontrado.");
            }
            else {
                logger.debug(fileName + '- writeSingleNode - Tipo de dato inválido.');
                resolve("writeSingleNode - Tipo de dato inválido.");
            }
        }
    })
}

function createSubscription(datosConexion) {
    logger.debug(fileName + ' - createSubscription - ', datosConexion);
    let sessionAux = sessions.find(data => data.idClient == datosConexion.id);
    let subscriptionAux = subscriptions.find(data => data.idClient == datosConexion.id && data.nameSubscription == datosConexion.nameSubscription);

    return new Promise(async (resolve, reject) => {
        if(sessionAux && ! subscriptionAux) {
            try {
                const subscriptionOptions = {
                    maxNotificationsPerPublish: 1000,
                    publishingEnabled: true,
                    requestedLifetimeCount: 100,
                    requestedMaxKeepAliveCount: 1,
                    requestedPublishingInterval: 1000
                };
                sessionAux.session.createSubscription2(subscriptionOptions, (err, subscription) => {
                    if(err) {
                        logger.error(fileName + '- createSubscription - ', err);
                        reject(err);
                    }
                    subscription
                    .on("started", () => {
                        logger.debug(fileName + '- createSubscription - Subscription started id =', datosConexion);
                    })
                    .on("keepalive", function() {
                        // logger.debug(fileName + "- createSubscription - "+"subscription keepalive");
                    })
                    .on("terminated", function() {
                        logger.debug(fileName + '- createSubscription - Subscription terminated id=', datosConexion);
                     });

                    subscriptions.push({subscription: subscription, idClient: datosConexion.id, nameSubscription: datosConexion.nameSubscription})
                    resolve('Subscripción creada correctamente ' + datosConexion.nameSubscription);
                });
            }
            catch (error) {
                logger.error(fileName + '- createSubscription - ', error);
                reject(error);
            }
            finally {
            }
        }
        else {
            if(!sessionAux) {
                logger.debug(fileName +' - createSubscription - Session no encontrada.');
                resolve("Session no encontrada.");
            }
            else if(sessionAux && subscriptionAux) {
                logger.debug(fileName +' - createSubscription - Session y Subscription encontrados.');
                resolve("createSubscription - Session y Subscription encontrados");
            }
        }
    })
}

function monitorLogger(datosConexion) {
    logger.debug(fileName +' - monitorLogger - ', datosConexion);
    let opcMiddleware = require("./controllers/opcMiddleware");
    let HistoricoProceso = require('./models/HistoricoProceso/HistoricoProceso');
    let HistoricoEstado = require('./models/HistoricoEstado/HistoricoEstado');
    let HistoricoResponsable = require('./models/HistoricoResponsable/HistoricoResponsable');
	let HistoricoArticulo = require('./models/HistoricoArticulo/HistoricoArticulo');
    
    let dato = subscriptions.find(data => data.idClient == datosConexion.IdOpcServer && data.nameSubscription == datosConexion.nameSubscription);

    return new Promise(async (resolve, reject) => {
        if(dato) {
            if(datosConexion.IdTipoTag == 12 || datosConexion.IdTipoTag == 9 || datosConexion.IdTipoTag == 11 || datosConexion.IdTipoTag == 15 || datosConexion.IdTipoTag == 16) {
                try {
                    const itemToMonitor = {
                        nodeId: resolveNodeId(datosConexion.OpcTagNombre),
                        attributeId: AttributeIds.Value
                    };
                    const monitoringParamaters = {
                        samplingInterval: datosConexion.ScanTime, // samplingInterval: 15000,
                        discardOldest: true,
                        queueSize: 1
                    };
    
                    await dato.subscription.monitor(
                        itemToMonitor,
                        monitoringParamaters,
                        TimestampsToReturn.Both,
                        (err, monitoredItem) => {
                            monitoredItem
                            .on("changed", async function(dataValue) {
                                // Calidad buena de datos
                                if(dataValue.statusCode.value == 0) {
                                    // Tipo de tag Proceso
                                    if(datosConexion.IdTipoTag == 12) {
                                        HistoricoProceso.save({
                                            IdTag: datosConexion.Id,
                                            Valor: dataValue.value.value,
                                            Calidad: 192,
                                            Fecha: funciones.getDateTime(JSON.parse(JSON.stringify(dataValue.sourceTimestamp))),
                                            Procesado: 0
                                        })
                                    }
                                    // Tipo de tag Responsable
                                    else if(datosConexion.IdTipoTag == 11) {
                                        let estado = await HistoricoResponsable.save({
                                            IdTag: datosConexion.Id,
                                            Valor: dataValue.value.value,
                                            Calidad: 192,
                                            Fecha: funciones.getDateTime(JSON.parse(JSON.stringify(dataValue.sourceTimestamp))),
                                            Procesado: 0
                                        })
                                    }
									// Tipo de tag Articulo o ArticuloCodigo
                                    else if(datosConexion.IdTipoTag == 15 || datosConexion.IdTipoTag == 16) {
                                        let estado = await HistoricoArticulo.save({
                                            IdTag: datosConexion.Id,
                                            Valor: dataValue.value.value,
                                            Calidad: 192,
                                            Fecha: funciones.getDateTime(JSON.parse(JSON.stringify(dataValue.sourceTimestamp))),
                                            Procesado: 0
                                        })
                                    }
                                    // Tipo de tag Estado
                                    else if(datosConexion.IdTipoTag == 9) {
                                        let estado = await HistoricoEstado.save({
                                            IdTag: datosConexion.Id,
                                            Valor: dataValue.value.value,
                                            Calidad: 192,
                                            Fecha: funciones.getDateTime(JSON.parse(JSON.stringify(dataValue.sourceTimestamp))),
                                            Procesado: 0
                                        })
                                        // Evento DTM para guardado de Histórico
                                        opcMiddleware.eventoDTM(JSON.parse(JSON.stringify(estado)))
                                    }
                                }
                                // Calidad mala de datos
                                else {
                                    // Tipo de tag Proceso
                                    if(datosConexion.IdTipoTag == 12) {
                                        HistoricoProceso.save({
                                            IdTag: datosConexion.Id,
                                            Valor: dataValue.value.value != null ? dataValue.value.value : 0,
                                            Calidad: 0,
                                            Fecha: funciones.getDateTime(JSON.parse(JSON.stringify(dataValue.sourceTimestamp))),
                                            Procesado: 0
                                        })
                                    }
                                    // Tipo de tag Responsable
                                    else if(datosConexion.IdTipoTag == 11) {
                                        let estado = HistoricoResponsable.save({
                                            IdTag: datosConexion.Id,
                                            Valor: dataValue.value.value != null ? dataValue.value.value : 0,
                                            Calidad: 0, // dataValue.statusCode.value, 2023-07-19 CC: El statusCode es un valor long y no puede insertarse en el campo de la DB
                                            Fecha: funciones.getDateTime(JSON.parse(JSON.stringify(dataValue.sourceTimestamp))),
                                            Procesado: 0
                                        })
                                    }
									 // Tipo de tag Articulo o ArticuloCodigo
                                    else if(datosConexion.IdTipoTag == 15 || datosConexion.IdTipoTag == 16) {
                                        let estado = HistoricoArticulo.save({
                                            IdTag: datosConexion.Id,
                                            Valor: dataValue.value.value != null ? dataValue.value.value : 0,
                                            Calidad: 0,// dataValue.statusCode.value, 2023-07-19 CC: El statusCode es un valor long y no puede insertarse en el campo de la DB
                                            Fecha: funciones.getDateTime(JSON.parse(JSON.stringify(dataValue.sourceTimestamp))),
                                            Procesado: 0
                                        })
                                    }
                                    // Tipo de tag Estado
                                    else if(datosConexion.IdTipoTag == 9) {
                                        let estado = HistoricoEstado.save({
                                            IdTag: datosConexion.Id,
                                            Valor: dataValue.value.value != null ? dataValue.value.value : 0,
                                            Calidad: 0, // dataValue.statusCode.value, 2023-07-19 CC: El statusCode es un valor long y no puede insertarse en el campo de la DB
                                            Fecha: funciones.getDateTime(JSON.parse(JSON.stringify(dataValue.sourceTimestamp))),
                                            Procesado: 0
                                        })
                                        // Evento DTM para guardado de Histórico
                                        opcMiddleware.eventoDTM(estado)
                                    }
                                }
                            })
                        }
                    );
                    logger.debug(fileName + ' - monitorLogger - Item agregado correctamente.');
                    resolve(datosConexion.OpcTagNombre + ' Incorporado correctamente');
                }
                catch (error) {
                    logger.error(fileName +' - monitorLogger - ', datosConexion, error);
                    resolve(error);
                }
            }
            else {
                resolve(false)
            }
        }
        else {
            resolve("Monitor - Suscripcion no encontrada");
        }
    })
}

function monitorTrigger(datosConexion) {
    logger.debug(fileName + ' - monitorTrigger - ', datosConexion);
    let opcMiddleware = require("./controllers/opcMiddleware");
    let dato = subscriptions.find(data => data.idClient == datosConexion.IdOpcServer && data.nameSubscription == datosConexion.nameSubscription);
    let monitorItem = monitorTriggerTags.find(data => data.idClient == datosConexion.IdOpcServer && data.nameSubscription == datosConexion.nameSubscription && data.nodeId == datosConexion.OpcTagNombre);
    return new Promise(async (resolve, reject) => {
        if(dato && !monitorItem) {
            try {
                const itemToMonitor = {
                    nodeId: resolveNodeId(datosConexion.OpcTagNombre),
                    attributeId: AttributeIds.Value
                };
                const monitoringParamaters = {
                    samplingInterval: datosConexion.ScanTime, //samplingInterval: 15000,
                    discardOldest: true,
                    queueSize: 1
                };

                dato.subscription.monitor(
                    itemToMonitor,
                    monitoringParamaters,
                    TimestampsToReturn.Both,
                    (err, monitoredItem) => {
                        monitoredItem.on("changed", function(dataValue) {
                            // Calidad buena de datos
                            let Data = {
                                Nombre: datosConexion.Nombre,
                                Descripcion: datosConexion.Descripcion,
                                Tag: datosConexion.Id,
                                Valor: dataValue.value.value != null ? dataValue.value.value : 0,
                                Calidad: dataValue.statusCode.value,
                                Fecha: funciones.getDateTime(JSON.parse(JSON.stringify(dataValue.sourceTimestamp)))
                            } 
                            logger.debug(fileName + '- monitorTrigger - changed: ', Data)

                            opcMiddleware.updateMonitorTrigger(Data);
                        });
                    }
                );
                monitorTriggerTags.push({idClient: datosConexion.IdOpcServer, nameSubscription: datosConexion.nameSubscription, nodeId: datosConexion.OpcTagNombre})
                logger.debug(fileName + '- monitorTrigger - Incorporado correctamente')
                resolve({
                    estado: true, 
                    mensaje: datosConexion.OpcTagNombre + ' Incorporado correctamente'
                })
            }
            catch (error) {
                logger.error(fileName +' - monitorTrigger - ', datosConexion, error)
                resolve({
                    estado: false,
                    mensaje: error
                })
            }
            finally {
            }
        }
        else {
            if(!dato) {
                logger.debug(fileName + '- monitorTrigger - Suscripción no encontrada.')
                resolve({
                    estado: false,
                    mensaje:"Monitor - Suscripcion no encontrada"
                })
            }
            else if(dato && monitorItem) {
                logger.debug(fileName + '- monitorTrigger - ya está incorporado.')
                resolve({
                    estado: true,
                    mensaje: datosConexion.OpcTagNombre + ' Ya esta incorporado'
                })
            }
        }
    })
}

function monitorRealTime(datosConexion) {
    logger.debug(fileName + ' - monitorRealTime - ', datosConexion)
    let opcMiddleware = require("./controllers/opcMiddleware")
    let dato = subscriptions.find(data => data.idClient == datosConexion.IdOpcServer && data.nameSubscription == datosConexion.nameSubscription)

    return new Promise(async (resolve, reject) => {
        if(dato) {
            try {
                const itemToMonitor = {
                    nodeId: resolveNodeId(datosConexion.OpcTagNombre),
                    attributeId: AttributeIds.Value
                };
                const monitoringParamaters = {
                    samplingInterval: datosConexion.ScanTime, //samplingInterval: 15000, 
                    discardOldest: true,
                    queueSize: 1
                };

                dato.subscription.monitor(
                    itemToMonitor,
                    monitoringParamaters,
                    TimestampsToReturn.Both,
                    (err, monitoredItem) => {
                        monitoredItem.on("changed", function(dataValue) {
                            let Data = {
                                OpcTagNombre: datosConexion.OpcTagNombre, 
                                Valor: dataValue.value.value != null ? dataValue.value.value : 0,
                                Calidad: dataValue.statusCode.value,
                                Fecha: funciones.getDateTime(JSON.parse(JSON.stringify(dataValue.sourceTimestamp))),
                                Procesado: 0
                            }
                            opcMiddleware.updateMonitorValue(Data)
                        });
                    }
                );
                logger.debug(fileName + '- monitorRealTime - Incorporado correctamente.')
                resolve(datosConexion.OpcTagNombre + ' Incorporado correctamente')
            }
            catch (error) {
                logger.error(fileName +' - monitorRealTime - ', datosConexion, error)
                resolve(error)
            }
        }
        else {
            logger.error(fileName +' - monitorRealTime - Monitor - Suscripción no encontrada.')
            resolve("Monitor - Suscripcion no encontrada")
        }
    })
}

function terminateSubscription(datosConexion) {
    let dato = sessions.find(data => data.idClient == datosConexion.id)
    let subscriptionAux = subscriptions.find(data => data.idClient == datosConexion.id && data.nameSubscription == datosConexion.nameSubscription)
    let subscriptionAuxIndex = subscriptions.findIndex(data => data.idClient == datosConexion.id && data.nameSubscription == datosConexion.nameSubscription)

    return new Promise(async (resolve, reject) => {
        if(dato && subscriptionAux) {
            subscriptionAux.subscription.terminate()
            subscriptions.splice(subscriptionAuxIndex, 1)
            resolve("terminateSubscription - Subscription finalizada")
        }
        else {
            if(!dato) {
                reject("terminateSubscription - Session no encontrada")
            }
            else if(dato && !subscriptionAux) {
                resolve("terminateSubscription - Subscription no encontrada")
            }
        }
    })
}

function terminateSession(datosConexion) {
    let dato = clients.find(data => data.id == datosConexion.id)
    let sessionAux = sessions.find(data => data.idClient == datosConexion.id)
    let sessionAuxIndex = sessions.findIndex(data => data.idClient == datosConexion.id)

    return new Promise(async (resolve, reject) => {
        if(dato && sessionAux) {
            sessionAux.session.close(function(err) {
                if(err) {
                    logger.debug(fileName + "- terminateSession - " + "error clossing session")
                    reject("terminateSession - error clossing session")
                }
                else {
                    sessions.splice(sessionAuxIndex, 1)
                    resolve("terminateSession - Subscription finalizada")
                }
            })
            dato.client.disconnect(function(){})
        }
        else {
            if(!dato) {
                reject("terminateSession - Client no encontrada")
            }
            else if(dato && !sessionAux) {
                resolve("terminateSession - Session no encontrada")
            }
        }
    })
}

module.exports = {
    sessions,
    subscriptions,
    clients,
    terminateSubscription,
    terminateSession,
    connect,
    monitorLogger,
    monitorRealTime,
    monitorTrigger,
    writeSingleNode,
    createSession,
    createSubscription,
    getData
}