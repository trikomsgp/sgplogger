const { Sequelize, DataTypes } = require('sequelize');
let { mssql } = require('./configuraciones/config')
let sequelize 


module.exports = {
    init: () => {
        sequelize = new Sequelize(mssql.database, mssql.user, mssql.password, {
            host: mssql.server,
            dialect: 'mssql',
            dialectOptions: {
              // Observe the need for this nested `options` field for MSSQL
              options: {
                // Your tedious options here
                instanceName: mssql.instanceName,
                trustedConnection: true,
              }
            }
        });

        return sequelize
    },
    getIO: () => {
        if(!sequelize){
            throw new Error('Sequelize no fue inicializado')
        }
        else{
            return sequelize;
        }
    }
}