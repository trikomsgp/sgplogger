
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    
    return i;
}
function addDoubleZero(i) {
    if (i < 10) {
        i = "00" + i;
    }
    else if(i>= 10 && i<100){
        i = "0" + i;
    }
    
    return i;
}
module.exports ={
    getDateTime(date){
        let registro =  new Date(date);
        let fechaString = `${registro.getFullYear()}-${addZero(registro.getMonth()+1)}-${addZero(registro.getDate())} ${addZero(registro.getHours())}:${addZero(registro.getMinutes())}:${addZero(registro.getSeconds())}.${addDoubleZero(registro.getMilliseconds())}`//
        return fechaString
    },
}