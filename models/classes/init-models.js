var DataTypes = require("sequelize").DataTypes;
var _HistoricoEstado = require("./HistoricoEstado");
var _HistoricoProceso = require("./HistoricoProceso");
var _HistoricoResponsable = require("./HistoricoResponsable");
var _HistoricoArticulo = require("./HistoricoArticulo");
var _OpcServer = require("./OpcServer");
var _Tag = require("./Tag");
var _TipoTag = require("./TipoTag");

function initModels(sequelize) {
  var HistoricoEstado = _HistoricoEstado(sequelize, DataTypes);
  var HistoricoProceso = _HistoricoProceso(sequelize, DataTypes);
  var HistoricoResponsable = _HistoricoResponsable(sequelize, DataTypes);
  var HistoricoArticulo = _HistoricoArticulo(sequelize, DataTypes);
  var OpcServer = _OpcServer(sequelize, DataTypes);
  var Tag = _Tag(sequelize, DataTypes);
  var TipoTag = _TipoTag(sequelize, DataTypes);

  Tag.belongsTo(OpcServer, { as: "IdOpcServer_OpcServer", foreignKey: "IdOpcServer"});
  OpcServer.hasMany(Tag, { as: "Tags", foreignKey: "IdOpcServer"});
  HistoricoEstado.belongsTo(Tag, { as: "IdTag_Tag", foreignKey: "IdTag"});
  Tag.hasMany(HistoricoEstado, { as: "HistoricoEstados", foreignKey: "IdTag"});
  HistoricoProceso.belongsTo(Tag, { as: "IdTag_Tag", foreignKey: "IdTag"});
  Tag.hasMany(HistoricoProceso, { as: "HistoricoProcesos", foreignKey: "IdTag"});
  HistoricoResponsable.belongsTo(Tag, { as: "IdTag_Tag", foreignKey: "IdTag"});
  Tag.hasMany(HistoricoResponsable, { as: "HistoricoResponsables", foreignKey: "IdTag"});
  HistoricoArticulo.belongsTo(Tag, { as: "IdTag_Tag", foreignKey: "IdTag"});
  Tag.hasMany(HistoricoArticulo, { as: "HistoricoArticulos", foreignKey: "IdTag"});
  Tag.belongsTo(TipoTag, { as: "IdTipoTag_TipoTag", foreignKey: "IdTipoTag"});
  TipoTag.hasMany(Tag, { as: "Tags", foreignKey: "IdTipoTag"});

  return {
    HistoricoEstado,
    HistoricoProceso,
    HistoricoResponsable,
	HistoricoArticulo,
    OpcServer,
    Tag,
    TipoTag,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
