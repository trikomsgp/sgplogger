const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('HistoricoProceso', {
    Id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    IdTag: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Tag',
        key: 'Id'
      }
    },
    Valor: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    Calidad: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    Fecha: {
      type: DataTypes.STRING,
      allowNull: false
    },
    Procesado: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'HistoricoProceso',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "IX_HistoricoProceso",
        fields: [
          { name: "Fecha", order: "DESC" },
        ]
      },
      {
        name: "PK_HistoricoProceso",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
