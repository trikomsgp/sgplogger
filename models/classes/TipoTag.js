const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('TipoTag', {
    Id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Nombre: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    Activo: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    Version: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'TipoTag',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_TipoTag",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
