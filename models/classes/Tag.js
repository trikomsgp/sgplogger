const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Tag', {
    Id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Nombre: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    IdOpcServer: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'OpcServer',
        key: 'Id'
      }
    },
    OpcTagNombre: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    ScanTime: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    Descripcion: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    IdTipoTag: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'TipoTag',
        key: 'Id'
      }
    },
    TagGroup: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    Overflow: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    Version: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    Activo: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'Tag',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_Tag",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
