const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('OpcServer', {
    Id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Nombre: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    Descripcion: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    Tipo: {
      type: DataTypes.STRING(32),
      allowNull: false
    },
    Hostname: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    Conexion: {
      type: DataTypes.STRING(32),
      allowNull: false
    },
    Version: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    Activo: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'OpcServer',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_OpcServer",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
