const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('HistoricoEstado', {
    Id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    IdTag: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Tag',
        key: 'Id'
      }
    },
    Valor: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    Calidad: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    Fecha: {
      type: DataTypes.STRING,
      allowNull: false
    },
    Procesado: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    sequelize,
    tableName: 'HistoricoEstado',
    schema: 'dbo',
    timestamps: false,
    indexes: [
      {
        name: "PK_HistoricoEstado",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
