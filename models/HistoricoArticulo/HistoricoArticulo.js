// Variables del sequelize
var initModels = require('../classes/init-models')

// Variables del Log4JS
const logger = require('../../log4JS').getLogger()
const path = require('path')
const fileName = path.basename(__filename)

// Variables de conexión SQL
let sequelize = require('../../sequelize-mssql').getIO()

function getAll() {
    return new Promise(async (resolve, reject) => {
        try {
            logger.debug(`${fileName} - Inicio getAll()`)
            let models = initModels(sequelize)

            let tags = await models.Tag.findAll({
                where: {
                    Activo: true
                }
            })
            let tipoTag = await models.HistoricoArticulo.findAll()
            let result = []
            
            resolve(tags)
        } 
        catch (error) {
            console.error(`${fileName} - Error getAll()`, error);
            logger.error(`${fileName} - Error getAll()`, error)
            reject(error)
        }
        finally {
            logger.debug(`${fileName} - Fin getAll()`)
        }
    })    
}

function getById(data) {
    return new Promise(async (resolve, reject) => {
        try {
            logger.debug(`${fileName} - Inicio getById()`)

            let models = initModels(sequelize)
            let select = await models.HistoricoArticulo.findAll({
                where: {
                    Id: data,
                    Activo: true
                }
            })

            if(select.length > 0) { 
                resolve(select)
            }
            else {
                reject('No se encontró Id')
            }
        } 
        catch (error) {
            console.error(`${fileName} - Error getById()`, error);
            logger.error(`${fileName} - Error getById()`, error)
            reject(error)
        }
        finally {
            logger.debug(`${fileName} - Fin getById()`)
        }
    })
}

function save(data) {
    return new Promise(async (resolve, reject) => {
        try {
            logger.debug(`${fileName} - Inicio save()`)

            let models = initModels(sequelize)
            let test = await models.HistoricoArticulo.create(data)

            resolve(test)
        } 
        catch (error) {
            console.error(`${fileName} - Error save()`, error);
            logger.error(`${fileName} - Error save()`, error)
            reject(error)
        }
        finally {
            logger.debug(`${fileName} - Fin save()`)
        }
    })
}

function update(data) {
    return new Promise(async (resolve, reject) => {
        try {
            logger.debug(`${fileName} - Inicio update()`)

            let models = initModels(sequelize)
            let select = await models.Tag.findAll({
                where: {
                    Id: data.Id
                }
            })

            if(select.length > 0){
                let revision = await models.HistoricoArticulo.findAll({
                    where: {
                        IdArticulo: data.Id
                    }
                })
                // TODO: Corregir porque estos datos son para UPs (lo mismo en entidades anteriores)
                let newData = {
                    Nombre: data.Nombre,
                    Origen: data.Origen,
                    Codigo: data.Codigo,
                    Descripcion: data.Descripcion,
                    Version: select[0].Version + 1,
                }

                let update = await models.Tag.update(newData, {
                    where:{
                        Id: data.Id
                    }
                })
               
                resolve(update)
            }
            else {
                reject('No se encontró Id')
            }          
        } 
        catch (error) {
            console.error(`${fileName} - Error update()`, error);
            logger.error(`${fileName} - Error update()`, error)
            reject(error)
        }
        finally {
            logger.debug(`${fileName} - Fin update()`)
        }
    })    
}

function deleteById(data) {
    return new Promise(async (resolve, reject) => {
        try {
            logger.debug(`${fileName} - Inicio deleteById()`)

            let models = initModels(sequelize)
            let select = await models.HistoricoArticulo.findAll({
                where: {
                    Id: data.Id
                }
            })

            if(select.length > 0) {
                let newData = {
                    Activo: false
                }
                let update = await models.HistoricoArticulo.update(newData, {
                    where:{
                        Id: data.Id
                    }
                }) 
                resolve(update)
            }
            else {
                reject('No se encontró Id')
            }
        }
        catch (error) {
            console.error(`${fileName} - Error deleteById()`, error);
            logger.error(`${fileName} - Error deleteById()`, error)
            reject(error)
        }
        finally {
            logger.debug(`${fileName} - Fin deleteById()`)
        }
    })    
}

module.exports = {
    getAll,
    getById,
    save, 
    update,
    deleteById
}