let io;
let logger;
var log4js;

module.exports = {
    init: () => {
        log4js = require("log4js");
        log4js.configure({
            appenders: { app: { type: "file", layout: {type: "pattern", pattern: `[%d] [%p] [${process.pid}] %m`}, filename: "log/sgplogger.log", maxLogSize: 5000000, backups: 10, compress: false} },
            categories: { default: { appenders: ["app"], level: "error" } }
        });
        logger = log4js.getLogger('app');
        logger.level = "debug"; // default level is OFF - which means no logs at all.
        
        logger.debug("-------------------------------------");
        logger.debug("Inicio de Log");
        logger.debug("-------------------------------------");
        return logger;
    },
    getLogger: () => {
        if(!logger) {
            throw new Error('Socket.IO no fue inicializado.');
        }
        else {
            return logger;
        }
    }
}