

function terminate (server, options = { coredump: false, timeout: 500 }) {
    // Exit function+
    const exit = code => {
      options.coredump ? process.abort() : process.exit(code)
    }
  
    return (code, reason, logger4JS) => (err, promise) => {
    
      
    
      if (err && err instanceof Error) {
      // Log error information, use a proper logging library here :)
        console.log(err.message, err.stack)
      }
  
      // Attempt a graceful shutdown
      try {
        logger4JS.debug('Test', reason)
      }
      catch (error) {
        console.log('error', error)
      }
      console.log('Terminate', reason)


      server.close(exit)
      setTimeout(exit, options.timeout).unref()
    }
  }
  
  module.exports = terminate