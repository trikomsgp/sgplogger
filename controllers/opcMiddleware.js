const { middleware } = require('../configuraciones/config');
let opcClient = require('../OPCUAClient');
let globalAxios = require('axios');

// Variables del Log4JS
const log4JS = require('../log4JS');
const logger = log4JS.getLogger();
const path = require('path');
const fileName = path.basename(__filename);

// Dirección Middleware
urlMiddleware = middleware.url + ":" + middleware.port;

// updateMonitorValue
exports.updateMonitorValue = (data) => {
    globalAxios.post(urlMiddleware + '/OPCClient/updateMonitorValue', data)
    .then(axiosRes => {
        logger.debug(fileName + ' - updateMonitorValue - ok - ', JSON.stringify(data));
    })
    .catch(error => {
        logger.error(fileName + ' - updateMonitorValue - ' + ' ' + error);
    })
};

// monitorTriggers
exports.monitorTriggers = async (req, res, next) => {
    let data = req.body;
    let modelOPCClient = require('../models/OpcServer/OpcServer');
    let result = {
        Erroreos: [],
        Exitosos: []
    }
    
    try {
        if(Array.isArray(data)) {
            let resOpcServers = await modelOPCClient.getAll();

            if(Array.isArray(resOpcServers)) {
                for(let OpcServer of resOpcServers) {
                    let OPCServerAux = JSON.parse(JSON.stringify(OpcServer))
                    try {
                        let connectRes = await opcClient.connect({endpointUrl: OPCServerAux.Hostname, id: OPCServerAux.Id, nombre: OPCServerAux.Nombre})
                        logger.debug(fileName + ' - monitorTriggers - ', connectRes);
                        console.log('connectRes', connectRes);
                        let session = await opcClient.createSession({id: OPCServerAux.Id});
                        logger.debug(fileName + ' - monitorTriggers - ', session);
                        console.log('session', session);
                    } catch (error) {
                        logger.error(fileName + ' - monitorTriggers - ', error);
                    }
                }
            }
            for(let tag of data) {
                tag.nameSubscription = 'Trigger';
                let resSubscription = await opcClient.createSubscription({id: tag.IdOpcServer, nameSubscription: "Trigger"})
                console.log('resSubscription', resSubscription);

                let resTag = await opcClient.monitorTrigger(tag);

                if(resTag.estado) {
                    result.Exitosos.push(`${tag.Nombre} ${tag.Descripcion}`);
                }
                else if(!resTag.estado) {
                    result.Erroreos.push(`${tag.Nombre} ${tag.Descripcion}`);
                }
                console.log('resTag', resTag);
            }
            console.log('result', result);
            logger.debug(fileName + ' - monitorTriggers - ', result);
            res.status(200).json(result);
        }
        else {
            res.status(400).json('Dato enviado no es Array.');
        }
    }
    catch (error) {
        logger.error(fileName + ' - monitorTriggers - ', error);
        res.status(400).json(error);
    }
}

// updateMonitorTrigger
exports.updateMonitorTrigger = (data) => {
    globalAxios.post(urlMiddleware + '/OPCClient/updateMonitorTrigger', data)
    .then(axiosRes => {
        logger.debug(fileName + '- updateMonitorTrigger - Respuesta del Middleware: ' + JSON.stringify(axiosRes.data));
    })
    .catch(error => {
        logger.error(fileName + '- updateMonitorTrigger - Middleware: ', error);
    })
};

// resetMonitorTriggers
exports.resetMonitorTriggers = () => {
    globalAxios.post(urlMiddleware +'/OPCClient/resetMonitorTriggers')
    .then(axiosRes => {
        logger.debug(fileName + '- resetMonitorTriggers - ' + JSON.stringify(axiosRes.data));
    })
    .catch(error => {
        logger.error(fileName + '- resetMonitorTriggers - ' + ' ' + error);
    })
};

// eventoDTM
exports.eventoDTM = (data) => {
    globalAxios.post(urlMiddleware + '/OPCClient/eventoDTM', data)
    .then(axiosRes => {
        logger.debug(fileName + '- eventoDTM - ' + JSON.stringify(axiosRes.data));
    })
    .catch(error => {
        logger.error(fileName + '- eventoDTM - ' + ' ' + error);
    })
};

// updateServerStatus
exports.updateServerStatus = (data) => {
    return new Promise(async (resolve, reject) => {
        globalAxios.post(urlMiddleware + '/OPCClient/updateServerStatus', data)
        .then(axiosRes => {
            resolve();
            logger.debug(fileName + ' - updateServerStatus - ok - ', JSON.stringify(data)); 
        })
        .catch(error => {
            logger.error(fileName + ' - updateServerStatus - ' + ' ' + error);
            resolve();
        })
    })
};

// ServiceStatus
exports.serviceStatus = (data) => {
    return new Promise(async (resolve, reject) => {
        globalAxios.post(urlMiddleware + '/OPCClient/SGPLogger_status', data)
        .then(axiosRes => {
            logger.debug(fileName + ' - updateServerStatus - ok - ', JSON.stringify(data)); 
            resolve();
        })
        .catch(error => {
            logger.error(fileName + ' - updateServerStatus - ' + ' ' + error);
            reject();
        })
    })
};