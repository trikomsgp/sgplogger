const { configuracion } = require('../configuraciones/config');
let opcClient = require('../OPCUAClient')

// Variables del Log4JS
const log4JS = require('../log4JS')
const logger = log4JS.getLogger()
const path = require('path');
const fileName = path.basename(__filename)

exports.getByTag = async(req, res, next) => {
    let modelOPCClient = require('../models/OpcServer/OpcServer')
    let data = { idClient: req.params.idClient, nodeId: req.params.nodeId}

    try {
        let OPCServerAux = await modelOPCClient.getById(data.idClient)
        let connectRes = await opcClient.connect({endpointUrl: OPCServerAux.Hostname, id: OPCServerAux.Id, nombre: OPCServerAux.Nombre})
        let session = await opcClient.createSession({id: OPCServerAux.Id})
        let resGetData = await opcClient.getData(data)
        
        res.status(200).json(resGetData);
        logger.debug(fileName + '- getByTag - ' + JSON.stringify(resGetData));
    }
    catch(error) {
        res.status(400).json(fileName + '- getByTag - ' + ' ' + error)
        logger.error(fileName + '- getByTag - ' + ' ' + error);
    }
};

exports.writeSingleNode = (req, res, next) => {
    opcClient.writeSingleNode(req.body)
    .then(axiosRes => {  
        res.status(200).json(axiosRes);
        logger.debug(fileName + '- writeSingleNode - ' + JSON.stringify(axiosRes.data));
    })
    .catch(error => {
        res.status(400).json(fileName + '- writeSingleNode - ' + ' ' + error)
        logger.error(fileName + '- writeSingleNode - ' + ' ' + error);
    })
};

exports.writeMultipleNode = async (req, res, next) => {
    let multiples = req.body
    let writeOk = []
    let writeFail = []
    if(Array.isArray(multiples)) {
        for(let node of multiples) {
            let resWSN = await opcClient.writeSingleNode(node)
            if(resWSN == true) {
                writeOk.push(node.TagNombre)
            }
            else {
                writeFail.push(node.TagNombre)
            }
        }
    }
    res.status(200).json({OK: writeOk, FALLA: writeFail});
    logger.debug(fileName + '- writeSingleNode - ' + JSON.stringify({OK: writeOk, FALLA: writeFail}));
    // .then(axiosRes => {  
    //     res.status(200).json(axiosRes);
    //     logger.debug(fileName + '- writeSingleNode - ' + JSON.stringify(axiosRes.data)); 
    // })
    // .catch(error => {
    //     res.status(400).json(fileName + '- writeSingleNode - ' + ' ' + error)
    //     logger.error(fileName + '- writeSingleNode - ' + ' ' + error); 
    // })
};

exports.subscription = (req, res, next) => {
    opcClient.createSubscription(req.body)
    .then(axiosRes => {
        res.status(200).json(axiosRes);
        logger.debug(fileName + '- subscription - ' + JSON.stringify(axiosRes.data));
    })
    .catch(error => {
        res.status(400).json(fileName + '- subscription - ' + ' ' + error)
        logger.error(fileName + '- subscription - ' + ' ' + error);
    })
};

exports.terminateSubscription = (req, res, next) => {
    opcClient.terminateSubscription(req.body)
    .then(axiosRes => {
        res.status(200).json(axiosRes);
        logger.debug(fileName + '- terminateSubscription - ' + JSON.stringify(axiosRes.data));
    })
    .catch(error => {
        res.status(400).json(fileName + '- terminateSubscription - ' + ' ' + error)
        logger.error(fileName + '- terminateSubscription - ' + ' ' + error);
    })
};

exports.terminateSession = (req, res, next) => {
    opcClient.terminateSession(req.body)
    .then(axiosRes => {
        res.status(200).json(axiosRes);
        logger.debug(fileName + '- terminateSession - ' + JSON.stringify(axiosRes.data));
    })
    .catch(error => {
        res.status(400).json(fileName + '- terminateSession - ' + ' ' + error)
        logger.error(fileName + '- terminateSession - ' + ' ' + error);
    })
};

exports.monitor = (req, res, next) => {
    opcClient.monitorRealTime(req.body)
    .then(axiosRes => {
        res.status(200).json(axiosRes);
        logger.debug(fileName + '- monitor - ' + JSON.stringify(axiosRes.data));
    })
    .catch(error => {
        res.status(400).json(fileName + '- monitor - ' + ' ' + error)
        logger.error(fileName + '- monitor - ' + ' ' + error);
    })
};

exports.connect = (req, res, next) => {
    opcClient.connect(req.body)
    .then(resConnect => {
        opcClient.createSession(req.body)
        .then(resSession => {
            res.status(200).json(resConnect);
            logger.debug(fileName + '- connect createSession - ' + JSON.stringify(resConnect.data));
        })
        .catch(error => {
            res.status(400).json(fileName + '- connect createSession - ' + ' ' + error)
            logger.error(fileName + '- connect createSession - ' + ' ' + error);
        })
    })
    .catch(error => {
        res.status(400).json(fileName + '- connect - ' + ' ' + error)
        logger.error(fileName + '- connect - ' + ' ' + error);
    })
};

exports.clients = (req, res, next) => {
    res.status(200).json(opcClient.clients);
};

exports.statusClients = (req, res, next) => {
    let cacheClients = require('../cache/cache').getClients()
    res.status(200).json(cacheClients);
};