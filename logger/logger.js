let Tags, OpcServers, OPCClient;
OPCClient = require('../OPCUAClient');

// Variables del Log4JS
const log4JS = require('../log4JS');
const logger = log4JS.getLogger();
const path = require('path');
const fileName = path.basename(__filename);
let clients = require('../cache/cache');

module.exports = {
    initServers: () => {
        return new Promise(async (resolve, reject) => {
            Tags = require('../models/Tag/Tag');
            OpcServers = require('../models/OpcServer/OpcServer');

            try {
                // SGP OPCServers
                let resOpcServers = await OpcServers.getAll();
                clients.saveClients(resOpcServers.map((c) => {
                    return {
                        id: c.Id,
                        nombre: c.Nombre,
                        state: false,
                        activo: c.Activo
                    }
                }))

                if(Array.isArray(resOpcServers)) {
                    for(let OpcServer of resOpcServers) {
                        let OPCServerAux = JSON.parse(JSON.stringify(OpcServer));
                        try {
                            // Connection
                            let connectRes = await OPCClient.connect({endpointUrl: OPCServerAux.Hostname, id: OPCServerAux.Id, nombre: OPCServerAux.Nombre});
                            logger.debug(fileName + ' - initServers - ', connectRes);
                            // Session
                            let session = await OPCClient.createSession({id: OPCServerAux.Id});
                            logger.debug(fileName + ' - initServers - ', session);
                        } catch (error) {
                            logger.error(fileName + ' - initServers - ', error); 
                        }
                    }
                }
                
                // SGP Tags
                let resTags = await Tags.getAll();

                if(Array.isArray(resTags)) {
                    for(let Tag of resTags) {
                        let TagAux = JSON.parse(JSON.stringify(Tag));
                        TagAux.nameSubscription = 'Logger';
                        // Tags Subscription
                        let resSubscription = await OPCClient.createSubscription({id: TagAux.IdOpcServer, nameSubscription: "Logger"});
                        logger.debug(fileName + ' - initServers - ', resSubscription);
                        // Tags Monitor
                        let resMonitor = await OPCClient.monitorLogger(TagAux);
                        logger.debug(fileName + ' - initServers - ', resMonitor);
                    }
                }
                resolve('Fin Init Server Logger');
            }
            catch (error) {
                logger.error(fileName + ' - initServers - ', error);
                reject('Error Logger')
            }
        })
    },
    initTags: () => {
        return new Promise(async (resolve, reject) => {
            Tags = require('../models/Tag/Tag')
        })
    },
    clients
}