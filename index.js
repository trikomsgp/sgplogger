const http = require('http');
const path = require('path');

const express = require('express');

const app = express();

const { logger, port }  =  require('./configuraciones/config');

app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: true}));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.setHeader('Content-type', 'application/json;charset=UTF-8');
    // res.setHeader('Content-Disposition', 'attachment; filename=quote.xlsx');
    next();
});


const log4JS = require('./log4JS').init();
const logger4JS = require('./log4JS').getLogger();

const opcMiddlewareController = require('./controllers/opcMiddleware');

const server = app.listen( port, function() {
    opcMiddlewareController.serviceStatus({SGPLogger_status: true})
    // console.log(`Server SGPLogger Node.js started on port ${port}`);
    // logger4JS.info(`Server SGPLogger Node.js started on port ${port}`);
});

// let opcClient = require('./OPCUAClient')
// opcClient.connect().then(data => {
//     //('connected', data)
//     opcClient.createSession().then(session => {
//         //('session created', session)
//     })
// })

// Routes 
let opcClientRoutes = require('./routes/opcClientService');
app.use('/opcClient', opcClientRoutes);

let opcMiddlewareRoutes = require('./routes/opcMiddleware');
app.use('/opcMiddleware', opcMiddlewareRoutes);

// Inicio
let seqMssql = require('./sequelize-mssql');
seqMssql.init();

if (logger.activo) {
    let logger = require('./logger/logger');
    logger.initServers()
    .then(resInit => {
        // (resInit)
        // logger.initTags()
    })
    .catch(error => {})
}


opcMiddlewareController.resetMonitorTriggers();

let cache = require('./cache/cache');


process.on('SIGTERM', async signal => {
    await logger4JS.debug(`SIGTERM - Process ${process.pid} received a SIGTERM signal.`);
    console.log(`Process ${process.pid} received a SIGTERM signal.`);
    // await opcMiddlewareController.updateServerStatus([])
    process.exit(0);
})
  
process.on('SIGINT', async signal => {
    await opcMiddlewareController.serviceStatus({SGPLogger_status: false})
    await opcMiddlewareController.updateServerStatus([]);
    await logger4JS.debug(`SIGINT - Process ${process.pid} has been interrupted.`);
    console.log(`Process ${process.pid} has been interrupted`);
    process.exit(0);
})

process.on('beforeExit', async function (code) {
    await opcMiddlewareController.serviceStatus({SGPLogger_status: false})
    await opcMiddlewareController.updateServerStatus([]);
    logger4JS.debug(`beforeExit - Process ${process.pid} has been interrupted.`);
    return console.log(`Process to beforeExit with code ${code}`);
});

process.on('exit', function (code) {
    logger4JS.debug(`exit - Process to exit with code ${code}`);
    return console.log(`Process to exit with code ${code}`);
});

// const terminate = require('./terminate')
// const exitHandler = terminate(server, {
//     coredump: false,
//     timeout: 500
//   })

//   process.on('uncaughtException', exitHandler(1, 'Unexpected Error', logger4JS))
//   process.on('unhandledRejection', exitHandler(1, 'Unhandled Promise', logger4JS))
//   process.on('SIGTERM', exitHandler(0, 'SIGTERM', logger4JS))
//   process.on('SIGINT', exitHandler(0, 'SIGINT', logger4JS))
//   process.on('beforeExit', exitHandler(0, 'beforeExit', logger4JS))
//   process.on('exit', exitHandler(0, 'exit', logger4JS))