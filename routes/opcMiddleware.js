const express = require('express')
const Controller = require('../controllers/opcMiddleware')
const router = express.Router();

// POST /feed/post
router.post('/monitorTrigger', Controller.monitorTriggers)

module.exports = router;