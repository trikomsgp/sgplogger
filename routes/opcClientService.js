const express = require('express');

const usuariosController = require('../controllers/opcClientService');

const router = express.Router();

// GET /feed/posts
router.get('/getByTag/?idClient=:idClient&nodeId=:nodeId', usuariosController.getByTag);



router.get('/client', usuariosController.clients);

router.get('/statusClients', usuariosController.statusClients);

// POST /feed/post
router.post('/connect', usuariosController.connect)

router.post('/writeSingleNode', usuariosController.writeSingleNode)

router.post('/writeMultipleNode', usuariosController.writeMultipleNode)

router.post('/subscription', usuariosController.subscription);

router.post('/terminateSubscription', usuariosController.terminateSubscription);

router.post('/terminateSession', usuariosController.terminateSession);

router.post('/monitor', usuariosController.monitor);

module.exports = router;